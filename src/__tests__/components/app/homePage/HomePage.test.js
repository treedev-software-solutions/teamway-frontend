import React from 'react';
import { render, screen } from '../../../../utils/testUtils';
import HomePage from "../../../../components/app/homePage/HomePage";

test('renders home page title', () => {
    render(<HomePage />);
    expect(screen.getByRole('heading')).toHaveTextContent('Test: Are you an introvert or an extrovert');
});

test('renders home page description', () => {
    render(<HomePage />);
    const descriptionElement = screen.getByText(/So do you consider yourself more of an introvert or an extrovert\? Take this test, put together with input from psychoanalyst Sandrine Dury, and find out!/i);
    expect(descriptionElement).toBeInTheDocument();
});

test('renders take test button', () => {
    render(<HomePage />);
    expect(screen.getByRole('button')).toHaveTextContent('Take test');
});
