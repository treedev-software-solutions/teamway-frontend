import {render, screen} from "../../../../utils/testUtils";
import React from "react";
import Result from "../../../../components/app/test/Result";
import personalityTypes from "../../../../enums/personalityTypes";

import intlMessagesEN from '../../../../i18n/locales/en.json';

test('render correct extrovert results ', () => {
    render(<Result result={personalityTypes.EXTROVERT}/>);
    expect(screen.getByRole('heading')).toHaveTextContent(intlMessagesEN['result.extrovert.title']);
    expect(screen.getByRole('article')).toHaveTextContent(intlMessagesEN['result.extrovert.message']);
});

test('render correct introvert results ', () => {
    render(<Result result={personalityTypes.INTROVERT}/>);
    expect(screen.getByRole('heading')).toHaveTextContent(intlMessagesEN['result.introvert.title']);
    expect(screen.getByRole('article')).toHaveTextContent(intlMessagesEN['result.introvert.message']);
});

test('render correct ambivert results ', () => {
    render(<Result result={personalityTypes.AMBIVERT}/>);
    expect(screen.getByRole('heading')).toHaveTextContent(intlMessagesEN['result.ambivert.title']);
    expect(screen.getByRole('article')).toHaveTextContent(intlMessagesEN['result.ambivert.message']);
});