import {render, screen, fireEvent} from "../../../../utils/testUtils";
import React from "react";
import Questions from "../../../../components/app/test/Questions";
import {applyMiddleware, createStore} from "redux";
import rootReducer from "../../../../reducers";
import {Provider} from "react-redux";
import requestState from "../../../../enums/requestState";
import thunkMiddleware from "redux-thunk";
import questions from "../../../../enums/questions";
import answers from "../../../../enums/answers";

test('test empty question list case', () => {
    const mockStoreState = {
        questions: {
            state: requestState.SUCCESS,
            error: null,
            data: []
        }
    }
    const middlewares = [thunkMiddleware];
    const store = createStore(rootReducer, mockStoreState, applyMiddleware(...middlewares));

    render(
        <Provider store={store} >
            <Questions />
        </Provider>
    );
    const errorMessage = screen.getByText(/This test is not available at the moment! Please try again later\./i);
    expect(errorMessage).toBeInTheDocument();
});

test('renders correct question', () => {
    const mockStoreState = {
        questions: {
            state: requestState.SUCCESS,
            error: null,
            data: [{
                question: questions.QUESTION4,
                answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
            }]
        }
    }
    const middlewares = [thunkMiddleware];
    const store = createStore(rootReducer, mockStoreState, applyMiddleware(...middlewares));

    render(
        <Provider store={store} >
            <Questions />
        </Provider>
    );
    const question = screen.getByText(/You are taking part in a guided tour of a museum\. You:/i);
    expect(question).toBeInTheDocument();
});

test('hitting the next button changes to the next question',  () => {
    const mockStoreState = {
        questions: {
            state: requestState.SUCCESS,
            error: null,
            data: [
                {
                    question: questions.QUESTION1,
                    answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
                },
                {
                    question: questions.QUESTION2,
                    answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
                }
            ]
        }
    }
    const middlewares = [thunkMiddleware];
    const store = createStore(rootReducer, mockStoreState, applyMiddleware(...middlewares));

    render(
        <Provider store={store} >
            <Questions />
        </Provider>
    );

    const nextButton = screen.getByText(/Next/i);
    fireEvent.click(nextButton);

    const question = screen.getByText(/You’ve been sitting in the doctor’s waiting room for more than 25 minutes\. You:/i);
    expect(question).toBeInTheDocument();
});