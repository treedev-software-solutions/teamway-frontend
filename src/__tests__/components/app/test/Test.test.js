import {render, screen} from "../../../../utils/testUtils";
import React from "react";
import {applyMiddleware, createStore} from "redux";
import rootReducer from "../../../../reducers";
import {Provider} from "react-redux";
import requestState from "../../../../enums/requestState";
import thunkMiddleware from "redux-thunk";
import Test from "../../../../components/app/test/Test";

test('renders loading indicator', () => {
    const mockStoreState = {
        questions: {
            state: requestState.PENDING,
            error: null,
            data: []
        }
    }
    const middlewares = [thunkMiddleware];
    const store = createStore(rootReducer, mockStoreState, applyMiddleware(...middlewares));

    render(
        <Provider store={store} >
            <Test />
        </Provider>
    );
    const loadingIndicator = screen.getByTestId('panel-load-indicator');
    expect(loadingIndicator).toBeInTheDocument();
});