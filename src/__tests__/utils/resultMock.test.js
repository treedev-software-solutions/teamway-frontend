import {getPersonalityType} from "../../utils/resultMock";
import personalityTypes from "../../enums/personalityTypes";
import questions from "../../enums/questions";
import answers from "../../enums/answers";


test('test result with empty answers', () => {
    const inputMock = {};
    const result = getPersonalityType(inputMock);
    expect(result).toEqual(personalityTypes.AMBIVERT);
});

test('test introvert results', () => {
    const inputMock = {
        [questions.QUESTION1]: answers.ANSWER1,
        [questions.QUESTION2]: answers.ANSWER1,
        [questions.QUESTION3]: answers.ANSWER1,
    };
    const result = getPersonalityType(inputMock);
    expect(result).toEqual(personalityTypes.INTROVERT);
});

test('test extrovert results', () => {
    const inputMock = {
        [questions.QUESTION1]: answers.ANSWER4,
        [questions.QUESTION2]: answers.ANSWER4,
        [questions.QUESTION3]: answers.ANSWER4,
    };
    const result = getPersonalityType(inputMock);
    expect(result).toEqual(personalityTypes.EXTROVERT);
});

test('test ambivert results', () => {
    const inputMock = {
        [questions.QUESTION1]: answers.ANSWER1,
        [questions.QUESTION2]: answers.ANSWER1,
        [questions.QUESTION3]: answers.ANSWER4,
        [questions.QUESTION4]: answers.ANSWER4,
    };
    const result = getPersonalityType(inputMock);
    expect(result).toEqual(personalityTypes.AMBIVERT);
});
