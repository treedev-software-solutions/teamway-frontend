import {getRandomInt} from "../../utils/math";


test('test random number between 0, 10', () => {
    const randomInt = getRandomInt(0, 10);
    expect(randomInt).toBeGreaterThanOrEqual(0);
    expect(randomInt).toBeLessThanOrEqual(10);
});

test('test random number for single value', () => {
    const randomInt = getRandomInt(5, 5);
    expect(randomInt).toEqual(5);
});
