const personalityTypes = {
    INTROVERT: 'INTROVERT',
    EXTROVERT: 'EXTROVERT',
    AMBIVERT: 'AMBIVERT',
};

export default personalityTypes;