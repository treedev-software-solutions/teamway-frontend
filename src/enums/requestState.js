const requestState = {
    PENDING: 'pending',
    ERROR: 'error',
    SUCCESS: 'success',
    NONE: 'none',
};

export default requestState;