import {loadQuestionsError, loadQuestionsPending, loadQuestionsSuccess} from "../../../actions/questions";
import {getRandomInt} from "../../../utils/math";
import * as errorCodes from '../../errorCodes';
import questions from "../../../enums/questions";
import answers from "../../../enums/answers";

const loadQuestions = () => {
    return dispatch => {
        dispatch(loadQuestionsPending());

        setTimeout(() => {
            const responseNumber = getRandomInt(0, 10);

            if (responseNumber === 0) {
                dispatch(loadQuestionsError(errorCodes.UNKNOWN_ERROR));
            } else {
                dispatch(loadQuestionsSuccess([
                    {
                        question: questions.QUESTION1,
                        answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
                    },
                    {
                        question: questions.QUESTION2,
                        answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
                    },
                    {
                        question: questions.QUESTION3,
                        answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
                    },
                    {
                        question: questions.QUESTION4,
                        answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
                    },
                    {
                        question: questions.QUESTION5,
                        answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
                    },
                    {
                        question: questions.QUESTION6,
                        answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
                    },
                    {
                        question: questions.QUESTION7,
                        answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
                    },
                    {
                        question: questions.QUESTION8,
                        answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
                    },
                    {
                        question: questions.QUESTION9,
                        answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
                    },
                    {
                        question: questions.QUESTION10,
                        answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
                    },
                    {
                        question: questions.QUESTION11,
                        answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
                    },
                    {
                        question: questions.QUESTION12,
                        answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
                    },
                    {
                        question: questions.QUESTION13,
                        answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
                    },
                    {
                        question: questions.QUESTION14,
                        answers: [answers.ANSWER1, answers.ANSWER2, answers.ANSWER3, answers.ANSWER4]
                    }
                ]));
            }

        }, 2000);
    }
};

export default loadQuestions;