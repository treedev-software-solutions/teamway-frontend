import {getRandomInt} from "../../../utils/math";
import * as errorCodes from '../../errorCodes';
import {getPersonalityType} from "../../../utils/resultMock";
import {getResultsError, getResultsPending, getResultsSuccess} from "../../../actions/results";

const loadQuestions = (data, successCallback, errorCallback) => {
    return dispatch => {
        dispatch(getResultsPending());

        setTimeout(() => {
            const responseNumber = getRandomInt(0, 10);

            if (responseNumber === 0) {
                dispatch(getResultsError(errorCodes.UNKNOWN_ERROR));

                if (errorCallback && typeof successCallback === 'function') {
                    errorCallback();
                }
            } else {
                dispatch(getResultsSuccess(getPersonalityType(data)));

                if (successCallback && typeof successCallback === 'function') {
                    successCallback();
                }
            }

        }, 2000);
    }
};

export default loadQuestions;