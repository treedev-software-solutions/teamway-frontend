import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import {FormattedMessage} from "react-intl";
import ErrorMessage from "../statusMessage/ErrorMessage";


const ErrorDialog = props => {
    const { open, onClose, message } = props;
    return (
        <Dialog
            open={open}
        >
            <DialogContent>
                <DialogContentText>
                    <ErrorMessage>
                        {message}
                    </ErrorMessage>
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button
                    onClick={onClose}
                    color="primary"
                >
                    <FormattedMessage id={'actions.ok'} />
                </Button>
            </DialogActions>
        </Dialog>
    );
};

ErrorDialog.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    message: PropTypes.string,
};

export default ErrorDialog;