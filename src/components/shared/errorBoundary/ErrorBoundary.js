import React from 'react';
import { FormattedMessage } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import Typography from "@material-ui/core/Typography";

const styles = (theme) => ({
    message: {
        color: theme.palette.error.main,
        width: '100%',
        textAlign: 'center',
        margin: theme.spacing(2)
    },
});

class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }

    static getDerivedStateFromError(error) {
        return { hasError: true };
    }

    componentDidCatch(error, errorInfo) {
        // log error
    }

    render() {
        const { classes } = this.props;

        if (this.state.hasError) {
            return (
                <Typography variant={'h4'} className={classes.message}>
                    <FormattedMessage id={'errors.somethingWentWrong'}/>
                </Typography>
            );
        }

        return this.props.children;
    }
}

export default withStyles(styles)(ErrorBoundary);