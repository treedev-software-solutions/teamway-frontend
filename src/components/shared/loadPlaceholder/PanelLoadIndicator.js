import React from "react";
import PropTypes from 'prop-types';
import {  makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import clsx from "clsx";


const useStyles = makeStyles((theme) => ({
    root: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 1290
    },
    loadingProgress: {
        color: theme.palette.primary.main,
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: theme.typography.pxToRem(-24),
        marginLeft: theme.typography.pxToRem(-24),
    },
    backdrop: {
        backgroundColor: theme.palette.divider,
    }
}));

const PanelLoadIndicator = (props) => {
    const { showBackdrop } = props;
    const classes = useStyles();
    return (
        <div
            className={clsx(classes.root, {[classes.backdrop]: showBackdrop})}
            data-testid={'panel-load-indicator'}
        >
            <CircularProgress size={48} className={classes.loadingProgress} />
        </div>
    );
};

PanelLoadIndicator.propTypes = {
    showBackdrop: PropTypes.bool
}

PanelLoadIndicator.defaultProps = {
    showBackdrop: true,
}

export default PanelLoadIndicator;