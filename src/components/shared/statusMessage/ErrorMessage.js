import React from "react";
import PropTypes from 'prop-types';
import {  makeStyles } from '@material-ui/core/styles';
import clsx from "clsx";


const useStyles = makeStyles((theme) => ({
    root: {
        color: theme.palette.error.main,
        textAlign: 'left',
        fontWeight: 'bold',
        fontSize: theme.typography.pxToRem(14),
        marginTop: theme.spacing(1),
        marginRight: theme.spacing(1),
        marginLeft: theme.spacing(1),
    },
}));

const ErrorMessage = props => {
    const { className } = props;
    const classes = useStyles();
    return (
        <div className={clsx(classes.root, className)}>
            {props.children}
        </div>
    );
};

ErrorMessage.propTypes = {
    className: PropTypes.string
}

ErrorMessage.defaultProps = {
    className: null,
}

export default ErrorMessage;