import React from "react";
import { IntlProvider } from 'react-intl';

import intlMessagesEN from '../../../i18n/locales/en.json';


const DEFAULT_LANGUAGE = 'en';

const LanguageContextProvider = (props) => {
    const { children } = props;
    const language = DEFAULT_LANGUAGE;


    const getLanguageFile = () => {
        return intlMessagesEN;
    };

    let i18nConfig = {
        locale: language,
        messages: getLanguageFile(),
    };


    return (
        <IntlProvider key={ i18nConfig.locale } locale={ i18nConfig.locale }  messages={ i18nConfig.messages }>
            {children}
        </IntlProvider>
    )
};

export default LanguageContextProvider;