import React from 'react';
import { createTheme, ThemeProvider } from "@material-ui/core/styles";
import CssBaseline from '@material-ui/core/CssBaseline';
import { Provider } from 'react-redux'
import { applyMiddleware, createStore } from 'redux'
import thunkMiddleware from 'redux-thunk';
import { BrowserRouter as Router } from "react-router-dom";

import rootReducer from '../../reducers'
import MainEntryPoint from "./MainEntryPoint";


const mainTheme = createTheme({
  palette: {
      primary: {
          main: '#d81b60',
          light: '#ff5c8d',
          dark: '#a00037',
      },
      secondary: {
          main: '#1976d2',
          light: '#63a4ff',
          dark: '#004ba0',
      },
      tradeMark: '#909090',
  },
  typography: {
      fontFamily: [
          '-apple-system',
          'BlinkMacSystemFont',
          'Segoe UI',
          'Roboto',
          'Oxygen',
          'Ubuntu',
          'Cantarell',
          'Fira Sans',
          'Droid Sans',
          'Helvetica Neue',
          'sans-serif'
      ],
      button: {
          textTransform: 'none'
      }
  },
});

const middlewares = [thunkMiddleware];
const store = createStore(rootReducer, {}, applyMiddleware(...middlewares));

const  App = () => {
  return (
      <>
        <CssBaseline />
        <ThemeProvider theme={mainTheme}>
          <Provider store={store}>
              <Router>
                <MainEntryPoint/>
              </Router>
          </Provider>
        </ThemeProvider>
      </>
  );
};

export default App;