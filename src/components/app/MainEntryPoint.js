import React from "react";
import { Route, Switch } from "react-router-dom";

import * as routes from '../routes/routes';
import LanguageContextProvider from "../shared/contextProviders/languageContextProvider";
import ErrorBoundary from "../shared/errorBoundary/ErrorBoundary";
import HomePage from "./homePage/HomePage";
import Test from "./test/Test";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    root: {
        backgroundColor: '#f6f6f6',
        minHeight: '100vh',
        display: 'flex'
    },
}));

const MainEntryPoint = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <LanguageContextProvider>
                <Switch>
                    <Route path={routes.TEST}>
                        <ErrorBoundary>
                            <Test/>
                        </ErrorBoundary>
                    </Route>
                    <Route path={routes.DEFAULT}>
                        <ErrorBoundary>
                            <HomePage />
                        </ErrorBoundary>
                    </Route>
                </Switch>
            </LanguageContextProvider>
        </div>
    );
};

export default MainEntryPoint;