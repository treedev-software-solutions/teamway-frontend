import React from 'react';
import {makeStyles} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import {useHistory} from 'react-router-dom';
import {FormattedMessage} from "react-intl";
import Button from "@material-ui/core/Button";
import * as routes from '../../routes/routes';

const useStyles = makeStyles((theme) => ({
    root: {
        color: theme.palette.primary.main,
        padding: theme.spacing(4),
        textAlign: 'center',
        [theme.breakpoints.up('md')]: {
            maxWidth: '50vw',
        },
        alignSelf: 'center',
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    title: {
        fontSize: theme.typography.pxToRem(56),
        [theme.breakpoints.down('md')]: {
            fontSize: theme.typography.pxToRem(46),
        },
        [theme.breakpoints.down('sm')]: {
            fontSize: theme.typography.pxToRem(40),
        },
        [theme.breakpoints.down('xs')]: {
            fontSize: theme.typography.pxToRem(30),
        },
    },
    message: {
        fontSize: theme.typography.pxToRem(24),
        color: '#666',
        marginTop: '2vw',
        marginBottom: '2vw',
        [theme.breakpoints.down('md')]: {
            fontSize: theme.typography.pxToRem(22),
        },
        [theme.breakpoints.down('sm')]: {
            fontSize: theme.typography.pxToRem(20),
        },
        [theme.breakpoints.down('xs')]: {
            marginTop: theme.spacing(4),
            marginBottom: theme.spacing(4),
            fontSize: theme.typography.pxToRem(18),
        },
    }
}));

const HomePage = () => {
    const classes = useStyles();
    const history = useHistory();

    const onTakeTestAction = () => {
        history.push(routes.TEST);
    }

    return (
        <div className={classes.root}>
            <Typography variant={'h3'} component={'h1'} className={classes.title}>
                <FormattedMessage id={'home.title'}/>
            </Typography>
            <Typography className={classes.message}>
                <FormattedMessage id={'home.message'}/>
            </Typography>
            <Button color={'primary'} variant={'outlined'} onClick={onTakeTestAction}>
                <FormattedMessage id={'actions.takeTest'}/>
            </Button>
        </div>
    )
}

export default HomePage;