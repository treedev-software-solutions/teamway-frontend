import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from "@material-ui/core";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import loadQuestions from "../../../api/actions/questions/loadQuestions";
import {getResult} from "../../../reducers/result";
import Result from "./Result";
import Questions from "./Questions";
import Paper from "@material-ui/core/Paper";
import ErrorBoundary from "../../shared/errorBoundary/ErrorBoundary";

const useStyles = makeStyles((theme) => ({
    root: {
        alignSelf: 'center',
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '100vw',
        [theme.breakpoints.up('md')]: {
            width: '80vw',
        },
        [theme.breakpoints.up('lg')]: {
            width: '50vw',
        },
    },
    mainContainer: {
        position: 'relative',
        width: '100%',
        minHeight: 400,
        backgroundColor: 'inherit',
        display: 'flex',
        height: '100vh',
        [theme.breakpoints.up('md')]: {
            height: '80vh',
        },
        [theme.breakpoints.up('lg')]: {
            height: '50vh',
        },
        [theme.breakpoints.down('sm')]: {
            border: 0,
            borderRadius: 0
        },
    }
}));

const Test = (props) => {
    const {
        loadQuestions,
        result,
    } = props;
    const classes = useStyles();

    useEffect(() => {
        loadQuestions();
    }, [loadQuestions])

    return (
        <div className={classes.root}>
            <Paper variant={'outlined'} className={classes.mainContainer}>
                {
                    result ? (
                        <ErrorBoundary>
                            <Result result={result} />
                        </ErrorBoundary>
                    ) : (
                        <ErrorBoundary>
                            <Questions/>
                        </ErrorBoundary>
                    )
                }
            </Paper>

        </div>
    )
}

Test.propTypes = {
    loadQuestions: PropTypes.func.isRequired,
    result: PropTypes.string,
};

Test.defaultProps = {
    result: null
}

const mapStateToProps = state => ({
    result: getResult(state),
});

const mapDispatchToProps = dispatch => bindActionCreators( {
    loadQuestions,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Test)