import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from "@material-ui/core";
import {FormattedMessage} from "react-intl";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(4),
        overflowY: 'auto'
    },
    title: {
        fontSize: theme.typography.pxToRem(20),
        color: theme.palette.primary.main,
        marginBottom: theme.spacing(2),
        fontWeight: 'normal'
    },
    description: {
        color: '#666',
    }
}));

const Result = (props) => {
    const {
        result
    } = props;
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Typography variant={'h1'} className={classes.title}>
                <FormattedMessage id={`result.${result.toLowerCase()}.title`}/>
            </Typography>
            <div className={classes.description} role={'article'}>
                <FormattedMessage id={`result.${result.toLowerCase()}.message`}/>
            </div>
        </div>
    )
}

Result.propTypes = {
    result: PropTypes.string.isRequired,
};

export default Result;