import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from "@material-ui/core";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import getResults from "../../../api/actions/results/getResults";
import {getQuestionList, getQuestionListError, isQuestionListPending} from "../../../reducers/questions";
import {getResultError, isResultPending} from "../../../reducers/result";
import PanelLoadIndicator from "../../shared/loadPlaceholder/PanelLoadIndicator";
import ErrorMessage from "../../shared/statusMessage/ErrorMessage";
import * as errorCodes from '../../../api/errorCodes';
import {FormattedMessage, useIntl} from "react-intl";
import Typography from "@material-ui/core/Typography";
import LinearProgress from "@material-ui/core/LinearProgress";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import Radio from "@material-ui/core/Radio";
import Button from "@material-ui/core/Button";
import ErrorDialog from "../../shared/genericDialogs/ErrorDialog";

const useStyles = makeStyles((theme) => ({
    root: {
        flex: 1,
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        [theme.breakpoints.down('xs')]: {
            marginBottom: 70
        },
    },
    questionListError: {
        display: 'flex',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    errorMessage: {
        fontWeight: 'bold'
    },
    navigation: {
        alignSelf: 'center',
        width: '90%',
        marginTop: theme.spacing(2),
        [theme.breakpoints.up('md')]: {
            width: '50%',
        },
    },
    questionContainer: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        minHeight: 0,
        [theme.breakpoints.up('sm')]: {
            padding: theme.spacing(2),
        },
    },
    progress: {
        margin: theme.spacing(2),
        height: 20
    },
    questionTitleBar: {
        fontSize: theme.typography.pxToRem(20),
        color: theme.palette.primary.main,
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    middleContainer: {
        padding: theme.spacing(2),
        flex: '1 1 auto',
        overflowY: 'auto'
    },
    question: {
        fontWeight: 'bold',
        marginBottom: theme.spacing(2),
    },
    actionRow: {
        paddingTop: theme.spacing(1),
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        display: 'flex',
        flexDirection: 'row',
        '& > :first-child': {
            marginRight: theme.spacing(2),
        },
        [theme.breakpoints.down('xs')]: {
            position: 'fixed',
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: theme.palette.common.white,
            boxShadow: '0 -5px 5px -5px #333',
            paddingTop: theme.spacing(2),
            paddingBottom: theme.spacing(2),
        },

    },
    finishButton: {
        marginLeft: 'auto'
    },
}));

const Questions = (props) => {
    const {
        getResults,
        isResultPending,
        isQuestionListPending,
        questionListError,
        resultError,
        questionList
    } = props;
    const classes = useStyles();
    const intl = useIntl();
    const [isErrorMessageVisible, setErrorMessageVisibility] = useState(false);

    const [testAnswers, setTestAnswers] = useState(()=>{
        const savedAnswers = localStorage.getItem('answers');

        if (savedAnswers) {
            return JSON.parse(savedAnswers);
        }

        return {};
    });
    const [activeStep, setActiveStep] = useState(() => {
        const activeQuestion = localStorage.getItem('activeQuestion');

        if (activeQuestion && !isNaN(activeQuestion)) {
            return parseInt(activeQuestion);
        }

        return 0;
    });

    const handleNext = () => {
        setActiveStep((prevActiveStep) => {
            const nextActiveStep = prevActiveStep + 1;
            localStorage.setItem('activeQuestion', nextActiveStep.toString());
            return nextActiveStep;
        });
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => {
            const nextActiveStep =prevActiveStep - 1;
            localStorage.setItem('activeQuestion', nextActiveStep.toString());
            return nextActiveStep;
        });
    };

    const setAnswer = (question, answer) => {
        setTestAnswers((currentAnswerList) => {
            const newAnswerList = {
                ...currentAnswerList,
                [question]: answer,
            };

            localStorage.setItem('answers', JSON.stringify(newAnswerList));

            return newAnswerList;
        });
    }

    const getQuestionListErrormessage = () => {
        switch (questionListError) {
            case errorCodes.UNKNOWN_ERROR:
                return (
                    <Typography className={classes.errorMessage}>
                        <FormattedMessage id={'errors.questionListError'}/>
                    </Typography>
                )
            default:
                return null;
        }
    }

    const getResultErrormessage = () => {
        switch (resultError) {
            case errorCodes.UNKNOWN_ERROR:
                return (
                    <Typography className={classes.errorMessage}>
                        <FormattedMessage id={'errors.resultError'}/>
                    </Typography>
                )
            default:
                return null;
        }
    }

    const onFinish = () => {
        getResults(testAnswers, onGetResultsSuccess, onGetResultError)
    }

    const onGetResultsSuccess = () => {
        localStorage.removeItem('activeQuestion');
        localStorage.removeItem('answers');
    }

    const onGetResultError = () => {
        setErrorMessageVisibility(true);
    }

    if (isQuestionListPending) {
        return <PanelLoadIndicator />;
    }

    return (
        <div className={classes.root}>
            {
                isResultPending && (
                    <PanelLoadIndicator />
                )
            }
            {
                questionListError ? (
                    <ErrorMessage className={classes.questionListError}>
                        {getQuestionListErrormessage()}
                    </ErrorMessage>
                ) : (
                    <>
                        {
                            questionList.length > 0 ? (
                                <div className={classes.questionContainer}>
                                    <LinearProgress className={classes.progress} variant="determinate" value={((activeStep + 1) * 100) / questionList.length }/>
                                    <div className={classes.questionTitleBar}>
                                        <span>
                                            <FormattedMessage id={'question.currentQuestion'} values={{current: activeStep + 1, all: questionList.length}} />
                                        </span>
                                        <span>
                                            {`${Math.round(((activeStep + 1) * 100) / questionList.length)}%`}
                                        </span>
                                    </div>
                                    <div className={classes.middleContainer}>
                                        <div className={classes.question}>
                                            <FormattedMessage id={`questions.${questionList[activeStep].question.toLowerCase()}`} />
                                        </div>
                                        <FormControl component="fieldset" className={classes.answers}>
                                            <RadioGroup aria-label="answers" value={testAnswers[questionList[activeStep].question] ? testAnswers[questionList[activeStep].question] : null} onChange={(event) => setAnswer(questionList[activeStep].question, event.target.value)}>
                                                {
                                                    questionList[activeStep].answers.map(answer => (
                                                        <FormControlLabel
                                                            key={answer}
                                                            value={answer}
                                                            control={<Radio />}
                                                            label={intl.formatMessage({id: `questions.${questionList[activeStep].question.toLowerCase()}.${answer.toLowerCase()}`})}
                                                        />
                                                    ))
                                                }
                                            </RadioGroup>
                                        </FormControl>
                                    </div>
                                    <div className={classes.actionRow}>
                                        <Button
                                            disabled={activeStep === 0}
                                            color={'primary'}
                                            onClick={handleBack}
                                        >
                                            <FormattedMessage id={'actions.previous'} />
                                        </Button>
                                        <Button
                                            variant={'contained'}
                                            color={'primary'}
                                            onClick={handleNext}
                                            disabled={activeStep === questionList.length -1}
                                        >
                                            <FormattedMessage id={'actions.next'} />
                                        </Button>
                                        {
                                            activeStep !== 0 && (
                                                <Button
                                                    color={'primary'}
                                                    className={classes.finishButton}
                                                    onClick={onFinish}
                                                >
                                                    <FormattedMessage id={'actions.finish'} />
                                                </Button>
                                            )
                                        }
                                    </div>
                                    <ErrorDialog
                                        open={isErrorMessageVisible}
                                        onClose={() => setErrorMessageVisibility(false)}
                                        message={getResultErrormessage()}
                                    />
                                </div>
                            ) : (
                                <ErrorMessage className={classes.questionListError}>
                                    <Typography className={classes.errorMessage}>
                                        <FormattedMessage id={'errors.questionListError'}/>
                                    </Typography>
                                </ErrorMessage>
                            )
                        }
                    </>
                )
            }
        </div>
    )
}

Questions.propTypes = {
    getResults: PropTypes.func.isRequired,
    questionList: PropTypes.array.isRequired,
    isResultPending: PropTypes.bool.isRequired,
    questionListError: PropTypes.string,
    resultError: PropTypes.string,
    isQuestionListPending: PropTypes.bool.isRequired,
};

Questions.defaultProps = {
    questionListError: null,
    resultError: null,
}


const mapStateToProps = state => ({
    questionList: getQuestionList(state),
    isResultPending: isResultPending(state),
    questionListError: getQuestionListError(state),
    resultError: getResultError(state),
    isQuestionListPending: isQuestionListPending(state),
});

const mapDispatchToProps = dispatch => bindActionCreators( {
    getResults
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Questions)