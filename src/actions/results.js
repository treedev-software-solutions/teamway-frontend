import * as actionTypes from "./types";

export const getResultsPending = () => {
    return {
        type: actionTypes.GET_RESULTS_PENDING
    }
};

export const getResultsError = (error) => {
    return {
        type: actionTypes.GET_RESULTS_ERROR,
        error,
    }
};

export const getResultsSuccess = (data) => {
    return {
        type: actionTypes.GET_RESULTS_SUCCESS,
        data
    }
};