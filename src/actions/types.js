export const LOAD_QUESTIONS_PENDING = 'load_questions_pending';
export const LOAD_QUESTIONS_SUCCESS = 'load_questions_success';
export const LOAD_QUESTIONS_ERROR = 'load_questions_error';

export const GET_RESULTS_PENDING = 'get_results_pending';
export const GET_RESULTS_SUCCESS = 'get_results_success';
export const GET_RESULTS_ERROR = 'get_results_error';