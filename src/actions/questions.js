import * as actionTypes from "./types";

export const loadQuestionsPending = () => {
    return {
        type: actionTypes.LOAD_QUESTIONS_PENDING
    }
};

export const loadQuestionsError = (error) => {
    return {
        type: actionTypes.LOAD_QUESTIONS_ERROR,
        error,
    }
};

export const loadQuestionsSuccess = (data) => {
    return {
        type: actionTypes.LOAD_QUESTIONS_SUCCESS,
        data
    }
};