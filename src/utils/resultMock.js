import questions from "../enums/questions";
import answers from "../enums/answers";
import personalityTypes from "../enums/personalityTypes";

const scoreMap = {
    [questions.QUESTION1]: {
        [answers.ANSWER1]: -2,
        [answers.ANSWER2]: -1,
        [answers.ANSWER3]: 1,
        [answers.ANSWER4]: 2,
    },
    [questions.QUESTION2]: {
        [answers.ANSWER1]: -2,
        [answers.ANSWER2]: -1,
        [answers.ANSWER3]: 1,
        [answers.ANSWER4]: 2,
    },
    [questions.QUESTION3]: {
        [answers.ANSWER1]: -2,
        [answers.ANSWER2]: -1,
        [answers.ANSWER3]: 1,
        [answers.ANSWER4]: 2,
    },
    [questions.QUESTION4]: {
        [answers.ANSWER1]: -2,
        [answers.ANSWER2]: -1,
        [answers.ANSWER3]: 1,
        [answers.ANSWER4]: 2,
    },
    [questions.QUESTION5]: {
        [answers.ANSWER1]: -2,
        [answers.ANSWER2]: -1,
        [answers.ANSWER3]: 1,
        [answers.ANSWER4]: 2,
    },
    [questions.QUESTION6]: {
        [answers.ANSWER1]: -2,
        [answers.ANSWER2]: -1,
        [answers.ANSWER3]: 1,
        [answers.ANSWER4]: 2,
    },
    [questions.QUESTION7]: {
        [answers.ANSWER1]: -2,
        [answers.ANSWER2]: -1,
        [answers.ANSWER3]: 1,
        [answers.ANSWER4]: 2,
    },
    [questions.QUESTION8]: {
        [answers.ANSWER1]: -2,
        [answers.ANSWER2]: -1,
        [answers.ANSWER3]: 1,
        [answers.ANSWER4]: 2,
    },
    [questions.QUESTION9]: {
        [answers.ANSWER1]: -2,
        [answers.ANSWER2]: -1,
        [answers.ANSWER3]: 1,
        [answers.ANSWER4]: 2,
    },
    [questions.QUESTION10]: {
        [answers.ANSWER1]: -2,
        [answers.ANSWER2]: -1,
        [answers.ANSWER3]: 1,
        [answers.ANSWER4]: 2,
    },
    [questions.QUESTION11]: {
        [answers.ANSWER1]: -2,
        [answers.ANSWER2]: -1,
        [answers.ANSWER3]: 1,
        [answers.ANSWER4]: 2,
    },
    [questions.QUESTION12]: {
        [answers.ANSWER1]: -2,
        [answers.ANSWER2]: -1,
        [answers.ANSWER3]: 1,
        [answers.ANSWER4]: 2,
    },
    [questions.QUESTION13]: {
        [answers.ANSWER1]: -2,
        [answers.ANSWER2]: -1,
        [answers.ANSWER3]: 1,
        [answers.ANSWER4]: 2,
    },
    [questions.QUESTION14]: {
        [answers.ANSWER1]: -2,
        [answers.ANSWER2]: -1,
        [answers.ANSWER3]: 1,
        [answers.ANSWER4]: 2,
    },
}

export const getPersonalityType = (answers) => {
    const personalityScore = Object.keys(answers).reduce((currentScore, question) => {
        currentScore += scoreMap[question][answers[question]];
        return currentScore;
    }, 0);

    if (personalityScore < -2) {
        return personalityTypes.INTROVERT;
    }

    if (personalityScore > 2) {
        return personalityTypes.EXTROVERT;
    }

    return personalityTypes.AMBIVERT;
}