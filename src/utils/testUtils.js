import React from 'react'
import { render as rtlRender } from '@testing-library/react'
import { IntlProvider } from 'react-intl'
import intlMessagesEN from '../i18n/locales/en.json';

const DEFAULT_LANGUAGE = 'en';

function render(ui, { locale = DEFAULT_LANGUAGE, ...renderOptions } = {}) {
    function Wrapper({ children }) {
        return <IntlProvider locale={locale} messages={ intlMessagesEN }>{children}</IntlProvider>
    }
    return rtlRender(ui, { wrapper: Wrapper, ...renderOptions })
}

export * from '@testing-library/react'

export { render }