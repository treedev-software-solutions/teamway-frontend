import * as actionTypes from '../actions/types'
import requestState from "../enums/requestState";

const initialState = {
    status: requestState.NONE,
    error: null,
    data: [],
};

const questions = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LOAD_QUESTIONS_PENDING: {
            return {
                ...state,
                data: [],
                status: requestState.PENDING,
                error: null,
            };
        }
        case actionTypes.LOAD_QUESTIONS_SUCCESS: {
            return {
                ...state,
                data: action.data,
                status: requestState.SUCCESS,
                error: null,
            };
        }
        case actionTypes.LOAD_QUESTIONS_ERROR:
            return {
                ...state,
                data: [],
                status: requestState.ERROR,
                error: action.error,
            };
        default:
            return state
    }
};

export default questions;

export const isQuestionListPending = state => state.questions.status === requestState.NONE || state.questions.status === requestState.PENDING;
export const getQuestionListError = state => state.questions.error;
export const getQuestionList = state => state.questions.data;
