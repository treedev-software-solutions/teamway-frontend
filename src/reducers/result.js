import * as actionTypes from '../actions/types'
import requestState from "../enums/requestState";

const initialState = {
    status: requestState.NONE,
    error: null,
    data: null,
};

const result = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_RESULTS_PENDING: {
            return {
                ...state,
                data: null,
                status: requestState.PENDING,
                error: null,
            };
        }
        case actionTypes.GET_RESULTS_SUCCESS: {
            return {
                ...state,
                data: action.data,
                status: requestState.SUCCESS,
                error: null,
            };
        }
        case actionTypes.GET_RESULTS_ERROR:
            return {
                ...state,
                data: null,
                status: requestState.ERROR,
                error: action.error,
            };
        default:
            return state
    }
};

export default result;

export const isResultPending = state => state.result.status === requestState.PENDING;
export const getResultError = state => state.result.error;
export const getResult = state => state.result.data;
