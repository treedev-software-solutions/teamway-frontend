import { combineReducers } from 'redux';
import questions from './questions';
import result from './result';

export default combineReducers({
    questions,
    result
});